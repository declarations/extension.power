(() => {
  /**
   * Check and set a global guard variable.
   * If this content script is injected into the same page again,
   * it will do nothing next time.
   */
  if (window.CSSInspectorHasRun) {
    return;
  }
  window.CSSInspectorHasRun = true;

  function getStyleSheetInformation() {
    const styleSheets = document.styleSheets
    return (styleSheets.length)
  };

  function resetSomething() {
    console.log('something got reset')
  }





  /*
      cyrb53a (c) 2023 bryc (github.com/bryc)
      License: Public domain. Attribution appreciated.
      The original cyrb53 has a slight mixing bias in the low bits of h1.
      This shouldn't be a huge problem, but I want to try to improve it.
      This new version should have improved avalanche behavior, but
      it is not quite final, I may still find improvements.
      So don't expect it to always produce the same output.
  */
  const cyrb53a = function(str, seed = 0) {
    let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
    for (let i = 0, ch; i < str.length; i++) {
      ch = str.charCodeAt(i);
      h1 = Math.imul(h1 ^ ch, 0x85ebca77);
      h2 = Math.imul(h2 ^ ch, 0xc2b2ae3d);
    }
    h1 ^= Math.imul(h1 ^ (h2 >>> 15), 0x735a2d97);
    h2 ^= Math.imul(h2 ^ (h1 >>> 15), 0xcaf649a9);
    h1 ^= h2 >>> 16; h2 ^= h1 >>> 16;
    return 2097152 * (h2 >>> 0) + (h1 >>> 11);
  };


  function hashCSSStylesheet(sheet) {
    let css = '';

    for (let cssRule of sheet.cssRules) {
      if (cssRule instanceof CSSStyleRule) {
        css += cssRule.style.cssText;
      }
    }

    return cyrb53a(css);
  }




  function indexSheetsOnPage() {
    /**
     * Log all stylesheets in current document.
     */
    const CSSIndex = [];

    for (let cssSheet of document.styleSheets) {
      try {
        if (cssSheet.cssRules.length > 0) {
          // Ensure there are rules. But also try ro read cssRules property
          // to trigger SecurityError on cross-domain sheets.

          let hash = hashCSSStylesheet(cssSheet);

          let sheet = {
            url: cssSheet.href, // URL of the sheet,
            pageUrl: window.location.toString(),
            pageHostname: window.location.hostname,
            selectorTexts: [], // List of selectors in the sheet
            selectors: [], // List of selectors in the sheet
            rules: [], // CSS Rules
            hash: hash
          }

          if (cssSheet.cssRules) {
            for (let cssRule of cssSheet.cssRules) {
              if (cssRule instanceof CSSStyleRule) {
                let rule = {
                  sheet: sheet, // Link to parent
                  selectorText: cssRule.selectorText, // raw selector
                  selectors: cssRule.selectorText.split(',').map(s => s.trim()),
                  declarations: {} // List of declarations
                }

                sheet.selectorTexts.push(rule.selectorText);
                sheet.selectors = sheet.selectors.concat(rule.selectors);

                for (let propName of cssRule.style) {
                  let value = cssRule.style[propName];
                  rule.declarations[propName] = value;
                }

                sheet.rules.push(rule);
              }
            }

            CSSIndex.push(sheet);
          }
        }
      }
      catch (error) {
        console.warn("Could not access rules of sheet ", error)
      }
    }
    return CSSIndex;
  }

  // loadCSSIndex();
  // indexSheetsOnPage();
  // storeCSSIndex();

  // console.log('hello');



  /**
   * Listen for messages from the background script.
   * Call "insertBeast()" or "removeExistingBeasts()".
   */
  browser.runtime.onMessage.addListener((message) => {
    if (message.command === "getInformation") {
      return Promise.resolve({
        response: indexSheetsOnPage()
      })
    } else if (message.command === "reset") {
      resetSomething();
    }
  });
})();
