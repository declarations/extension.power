/**
 * Listen for clicks on the buttons, and send the appropriate message to
 * the content script in the page.
 */
function onError(error) {
  console.error(`Error: ${error}`);
}

function readStorage() {
  console.log('he')
  browser.storage.local.get("stylesheets").then((res) => console.log(res)).catch(onError)
}

document.querySelector("#read-items").addEventListener("click", readStorage)

browser.storage.local.set({ "stylesheets": [] });

function listenForClicks() {
  document.addEventListener("click", (e) => {

    function getInformation(tabs) {
      browser.tabs.sendMessage(tabs[0].id, {
        command: "getInformation",
      }).then((response) => {
        console.log("Message from the content script:");
        const res = response.response


        browser.storage.local.get("stylesheets")
          .then(existingStorage => {
            let existingHashes;
            if (existingStorage.stylesheets) {
              existingHashes = new Set(existingStorage['stylesheets'].map(sheet => sheet.hash));
            } else {
              existingHashes = new Set()
              existingStorage.stylesheets = []
            }
            res.forEach(element => {
              if (!existingHashes.has(element.hash)) {
                existingStorage.stylesheets.push(element)
              };
            });
            // Update stored values
            browser.storage.local.set({
              "stylesheets": existingStorage.stylesheets
            });

            let cssIndex = initIndex(ruleIndexers, sheetIndexers);

            existingStorage.stylesheets.forEach((sheet) => {
              index(cssIndex, sheet, sheetIndexers);

              sheet.rules.forEach((rule) => {
                index(cssIndex, rule, ruleIndexers);
              });

            });

            let selectors = Object.keys(cssIndex['selector']);
            // Sort alphabetically
            selectors.sort()
            // Sort by length
            selectors.sort((a, b) => a.length - b.length)
            
            let selectorTexts = Object.keys(cssIndex['selectorText']);
            // Sort alphabetically
            selectorTexts.sort()
            // Sort by length
            selectorTexts.sort((a, b) => a.length - b.length);


            let declarationCounts = Object.values(cssIndex['selectorText']).flatMap(rules => rules.map(rule => Object.values(rule.declarations).length));
            // console.log(declarationCounts)
            let averageDeclarationCount = declarationCounts.reduce((sum, count) => sum + count, 0) / declarationCounts.length;
            let maxDeclarationCount = Math.max(...declarationCounts);

            Object.values(cssIndex['selectorText'])
              .forEach(rules => rules.map(rule => {
                let entries = Object.entries(rule.declarations);
                if (entries.length == maxDeclarationCount) {
                  console.log(rule.selectorText + '{\n  ' + entries.map(([prop, val]) => `${prop}: ${val};`).join('\n  ') + '\n}');
                }
              }));
            console.log()
            
            document.getElementById('stylesheet-stats--selector-count').innerText = selectors.length
            document.getElementById('stylesheet-stats--longest-selector').innerText = selectors[selectors.length-1];
            document.getElementById('stylesheet-stats--longest-selector-text').innerText = selectorTexts[selectorTexts.length-1];
            document.getElementById('stylesheet-stats--avg-declarations-per-rule').innerText = averageDeclarationCount;
            document.getElementById('stylesheet-stats--max-declarations-per-rule').innerText = maxDeclarationCount;
            document.getElementById('stylesheet-stats').removeAttribute('hidden');
            console.log(cssIndex);
          }).catch()

        //TODO update the ui to list the response
        // browser.storage.local.set({
        //   styleSheets: res,
        // })
        // // const resultContainer = document.querySelector(".inspection-results")
        // resultContainer.innerHTML = `<p>This webpage contains ${res} stylesheet${res > 1 ? "s" : ""}</p>`
      }
      ).catch(onError)
    }
    function reset() {

      browser.tabs.sendMessage(tabs[0].id, {
        command: "reset",
      })
    }


    if (e.target.tagName !== "BUTTON" || !e.target.closest("#popup-content")) {
      // Ignore when click is not on a button within <div id="popup-content">.
      return;
    }
    if (e.target.type === "reset") {
      browser.tabs
        .query({ active: true, currentWindow: true })
        .then(reset)
        .catch(reportError);
    } else {
      browser.tabs
        .query({ active: true, currentWindow: true })
        .then(getInformation)
        .catch(reportError);
    }

  });
}

/**
 * There was an error executing the script.
 * Display the popup's error message, and hide the normal UI.
 */
function reportExecuteScriptError(error) {
  document.querySelector("#popup-content").classList.add("hidden");
  document.querySelector("#error-content").classList.remove("hidden");
  console.error(`Failed to execute beastify content script: ${error.message}`);
}

/**
 * When the popup loads, inject a content script into the active tab,
 * and add a click handler.
 * If we couldn't inject the script, handle the error.
 */
browser.tabs
  .executeScript({ file: "/content_scripts/inspector.js" })
  .then(listenForClicks)
  .catch(reportExecuteScriptError);

/**
  Data index.

  sheets: [{
    url: str, // url of the sheet
    pageUrl: str, // url of the page the sheet was seen on
    pageHostname: str, // hostname of the page the sheet was seen on
    selectors: [ str:selector ],
    rules: [
      sheet: Sheet,
      selector: [],
      declarations: {
        property: value
      }
    ]
  }, ... ]

  indexes: {
    hash: { str:hash: [sheet, ...]}
    url: { str:url: [sheet, ...]}
    pageUrl: { str:url: [sheet, ...]}
    pageHostname: { str:hostname  : [sheet, ...]}
    selector: { str:selector: [rule, ...]}
    selectorCaseless: { str:selector: [rule, ...]}
    selectorText: { str:selector: [rule, ...]}
    selectorTextCaseless: { str:selector: [rule, ...]}
  }
*/

const ruleIndexers = {
  selectorText: (rule) => ([ rule.selectorText ]),
  selectorTextCaseless: (rule) => ([ rule.selectorText.toLowerCase() ]),
  selector: (rule) => rule.selectors,
  selectorCaseless: (rule) => rule.selectors.map(s => s.toLowerCase()),
}

const sheetIndexers = {
  hash: (sheet) => [ sheet.hash ],
  url: (sheet) => [ sheet.url ],
  pageUrl: (sheet) => [ sheet.pageUrl ],
  pageHostname: (sheet) => [ sheet.pageHostname ]
}

function index (index, value, indexers) {
  Object.entries(indexers).forEach(([ indexName, indexer ]) => {
    indexKeys = indexer(value);
    indexKeys.forEach(key => {
      if (!(key in index[indexName])) {
        index[indexName][key] = [];
      }
  
      index[indexName][key].push(value);
    });

  });
}

function initIndex () {
  let index = {};

  for (let i = 0; i < arguments.length; i++) {
    Object.keys(arguments[i]).forEach(name => {
      index[name] = {};
    })
  }

  return index;
}

// function loadCSSIndex () {
//   let sheetsInStorage = JSON.parse(window.localStorage.getItem('declarations:CSSIndex'));

//   if (sheetsInStorage !== null) {
//     CSSIndex.sheets = sheetsInStorage.map(
//       (sheet) => {
//         sheet.rules = sheet.rules.map(rule => { 
//           // Link rules back to the sheet
//           rule.sheet = sheet;
//           indexRule(rule);
  
//           return rule;
//         });
  
//         CSSIndex.indexes.hash[sheet.hash] = sheet;

//         return sheet;
//       }
//     );
//   }
// } 