/**
 * Log all stylesheets in current document.
 */
for (let sheet of document.styleSheets) {
  for (let rule of sheet.cssRules) {
    if (rule instanceof CSSStyleRule) {
      for (let propName of rule.style) {
        let value = rule.style[propName];
        console.log(propName, value);
      }
    }
  }
}