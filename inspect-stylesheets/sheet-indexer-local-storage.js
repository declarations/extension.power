/*
    cyrb53a (c) 2023 bryc (github.com/bryc)
    License: Public domain. Attribution appreciated.
    The original cyrb53 has a slight mixing bias in the low bits of h1.
    This shouldn't be a huge problem, but I want to try to improve it.
    This new version should have improved avalanche behavior, but
    it is not quite final, I may still find improvements.
    So don't expect it to always produce the same output.
*/
const cyrb53a = function(str, seed = 0) {
  let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
  for(let i = 0, ch; i < str.length; i++) {
    ch = str.charCodeAt(i);
    h1 = Math.imul(h1 ^ ch, 0x85ebca77);
    h2 = Math.imul(h2 ^ ch, 0xc2b2ae3d);
  }
  h1 ^= Math.imul(h1 ^ (h2 >>> 15), 0x735a2d97);
  h2 ^= Math.imul(h2 ^ (h1 >>> 15), 0xcaf649a9);
  h1 ^= h2 >>> 16; h2 ^= h1 >>> 16;
    return 2097152 * (h2 >>> 0) + (h1 >>> 11);
};


function hashCSSStylesheet (sheet) {
  let css = '';
  
  for (let cssRule of sheet.cssRules) {
    if (cssRule instanceof CSSStyleRule) {
      css += cssRule.style.cssText;
    }
  }

  return cyrb53a(css);
}

const CSSIndex = window.CSSIndex = {
  sheets: [],
  indexes: {
    hash: {},
    url: {},
    domain: {},
    selector: {},
    selectorCaseless: {},
    selectorText: {},
    selectorTextCaseless: {}
  }
}

/**
 * TODO: rewrite index as reducers so it's trivial to extend indexes
 */

/**
 * Adds the given rule to the index.
 * @param {Rule} rule 
 */
function indexRule(rule) {
  // Add to indexes
  if (!(rule.selector in CSSIndex.indexes.selectorText)) {
    CSSIndex.indexes.selectorText[rule.selector] = [];
  }

  CSSIndex.indexes.selectorText[rule.selector].push(rule);

  if (!(rule.selector.toLowerCase() in CSSIndex.indexes.selectorTextCaseless)) {
    CSSIndex.indexes.selectorTextCaseless[rule.selector.toLowerCase()] = [];
  }

  CSSIndex.indexes.selectorTextCaseless[rule.selector.toLowerCase()].push(rule);

  rule.selectors.forEach((selector) => {
    if (!(selector in CSSIndex.indexes.selector)) {
      CSSIndex.indexes.selector[selector] = [];
    }

    CSSIndex.indexes.selector[selector].push(rule);

    if (!(selector.toLowerCase() in CSSIndex.indexes.selectorCaseless)) {
      CSSIndex.indexes.selectorCaseless[selector.toLowerCase()] = [];
    }

    CSSIndex.indexes.selectorCaseless[selector.toLowerCase()].push(rule);
  });
}

function loadCSSIndex () {
  let sheetsInStorage = JSON.parse(window.localStorage.getItem('declarations:CSSIndex'));

  if (sheetsInStorage !== null) {
    CSSIndex.sheets = sheetsInStorage.map(
      (sheet) => {
        sheet.rules = sheet.rules.map(rule => { 
          // Link rules back to the sheet
          rule.sheet = sheet;
          indexRule(rule);
  
          return rule;
        });
  
        CSSIndex.indexes.hash[sheet.hash] = sheet;

        return sheet;
      }
    );
  }
} 

/**
 * Store sheets in localstorage, omit indexes
 */
function storeCSSIndex () {
  let sheetsToStore = CSSIndex.sheets.map((sheet) => {
    sheet.rules = sheet.rules.map(rule => { 
      rule.sheet = null;
      return rule;
    });

    return sheet;
  });


  window.localStorage.setItem('declarations:CSSIndex', JSON.stringify(sheetsToStore));
}

function indexSheetsOnPage () {
  /**
   * Log all stylesheets in current document.
   */
  for (let cssSheet of document.styleSheets) {
    try {
      if (cssSheet.cssRules.length > 0) {
        // Ensure there are rules. But also try ro read cssRules property
        // to trigger SecurityError on cross-domain sheets.

        let hash = hashCSSStylesheet(cssSheet);

        if (!(hash in CSSIndex.indexes.hash)) {
          let sheet = {
            url: cssSheet.href, // URL of the sheet
            selectors: [], // List of selectors in the sheet
            rules: [], // CSS Rules
            hash: hash
          }
      
          if (cssSheet.cssRules) {
            for (let cssRule of cssSheet.cssRules) {
              if (cssRule instanceof CSSStyleRule) {
                sheet.selectors.push(cssRule.selectorText);
                let rule = {
                  sheet: sheet, // Link to parent
                  selector: cssRule.selectorText, // raw selector
                  selectors: cssRule.selectorText.split(',').map(s => s.trim()),
                  declarations: {} // List of declarations
                }
                
                for (let propName of cssRule.style) {
                  let value = cssRule.style[propName];
                  rule.declarations[propName] = value;
                }
          
                sheet.rules.push(rule);
                indexRule(rule);
              }
            }
    
            CSSIndex.sheets.push(sheet);
            CSSIndex.indexes.hash[sheet.hash] = sheet;
          }
        }
        else {
          console.info('Already saw this sheet.', cssSheet);
        }
      }
    }
    catch (error) {
      console.warn("Could not access rules of sheet ", error)
    }
  }
}

loadCSSIndex();
indexSheetsOnPage();
storeCSSIndex();