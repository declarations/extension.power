/**
  Data index.

  sheets: [{
    url: str,
    selectors: [ str:selector ],
    rules: [
      sheet: Sheet,
      selector: [],
      declarations: {
        property: value
      }
    ]
  }, ... ]

  urlIndex: {
    str:url: [ sheet, ]
  }

  domainIndex: {
    str:url: [ sheet, ]
  }

  selectorIndex: {
    str:selector: [ Rule, ...]
  }


  selectorIndexCaseless: {
    str:selector: [ Rule, ...]
  }
*/

/**
 * Paste at in the console.
 */

const sheets = [],
      urlIndex = {},
      domainIndex = {},
      selectorIndex = {},
      selectorIndexCaseless = {},
      selectorIndexRaw = {},
      selectorIndexRawCaseless = {};

/**
 * Log all stylesheets in current document.
 */
for (let cssSheet of document.styleSheets) {
  let sheet = {
    url: cssSheet.href, // URL of the sheet
    selectors: [], // List of selectors in the sheet
    rules: [] // CSS Rules
  }

  try {
    if (cssSheet.cssRules) {
      for (let cssRule of cssSheet.cssRules) {
        if (cssRule instanceof CSSStyleRule) {
          sheet.selectors.push(cssRule.selectorText);
          let rule = {
            sheet: sheet, // Link to parent
            selector: cssRule.selectorText, // raw selector
            selectors: cssRule.selectorText.split(',').map(s => s.trim()),
            declarations: {} // List of declarations
          }
          
          for (let propName of cssRule.style) {
            let value = cssRule.style[propName];
            rule.declarations[propName] = value;
          }
    
          sheet.rules.push(rule);
    
          if (!(rule.selector in selectorIndexRaw)) {
            selectorIndexRaw[rule.selector] = [];
          }
    
          selectorIndexRaw[rule.selector].push(rule);

          if (!(rule.selector.toLowerCase() in selectorIndexRawCaseless)) {
            selectorIndexRawCaseless[rule.selector.toLowerCase()] = [];
          }
    
          selectorIndexRawCaseless[rule.selector.toLowerCase()].push(rule);

          rule.selectors.forEach((selector) => {
            if (!(selector in selectorIndex)) {
              selectorIndex[selector] = [];
            }
      
            selectorIndex[selector].push(rule);

            if (!(selector.toLowerCase() in selectorIndexCaseless)) {
              selectorIndexCaseless[selector.toLowerCase()] = [];
            }
      
            selectorIndexCaseless[selector.toLowerCase()].push(rule);
          });
        }
      }
    }
  }
  catch (error) {
    console.warn("Could not access rules of sheet ", sheet, error)
  }

  sheets.push(sheet);
}


selectors = Object.keys(selectorIndex);
selectors.sort()
// console.log(selectors);
selectors.sort((a, b) => a.length - b.length)
console.log(selectors)